from ghostshot.constants import RESOURCE_PREFIX
from gi.repository import Gtk, Adw
from ghostshot.confManager import ConfManager
from ghostshot.screenshot_helper import (
    grim_screenshot, grim_selection_screenshot
)


class MainUI(Adw.Bin):
    def __init__(self, window):
        super().__init__(vexpand=True, hexpand=True)
        self.confman = ConfManager()
        self.window = window
        self.builder = Gtk.Builder.new_from_resource(
            f'{RESOURCE_PREFIX}/ui/main_ui.ui'
        )
        self.main_box = self.builder.get_object('main_box')

        self.menu_btn = self.builder.get_object('menu_btn')
        self.menu_builder = Gtk.Builder.new_from_resource(
            f'{RESOURCE_PREFIX}/ui/menu.ui'
        )
        self.menu = self.menu_builder.get_object('generalMenu')
        self.popover = Gtk.PopoverMenu.new_from_model(self.menu)
        self.menu_btn.set_popover(self.popover)

        self.scrot_btn = self.builder.get_object('scrot_btn')
        self.scrot_btn.connect(
            'clicked', lambda *args: grim_screenshot(
                self.window, self.confman.conf['screenshots_folder']
            )
        )

        self.scrot_sel_btn = self.builder.get_object('scrot_sel_btn')
        self.scrot_sel_btn.connect(
            'clicked', lambda *args: grim_selection_screenshot(
                self.window, self.confman.conf['screenshots_folder']
            )
        )

        self.set_child(self.main_box)
        self.show()
