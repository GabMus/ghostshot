from ghostshot.constants import APP_ID, APP_NAME, PROFILE
from gi.repository import Gtk, Adw
from ghostshot.confManager import ConfManager
from ghostshot.main_ui import MainUI
from ghostshot.accel_manager import add_accelerators


class AppWindow(Adw.ApplicationWindow):
    def __init__(self):
        super().__init__()
        self.get_style_context().add_class('main-window')
        self.confman = ConfManager()

        self.set_title(APP_NAME)
        self.set_icon_name(APP_ID+'.dev' if PROFILE == 'development' else '')

        self.main_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)

        self.main_ui = MainUI(self)
        self.main_box.append(self.main_ui)

        self.set_content(self.main_box)

        def toggle_popover(*args):
            self.main_ui.menu_btn.popup()

        add_accelerators(
            self,
            [
                {
                    'combo': 'F10',
                    'cb': toggle_popover
                }
            ]
        )

        self.set_size_request(200, 100)
        self.set_default_size(
            self.confman.conf['windowsize']['width'],
            self.confman.conf['windowsize']['height']
        )

        self.confman.connect('dark_mode_changed', self.on_dark_mode_changed)

    def present(self, *args, **kwargs):
        super().present(*args, **kwargs)
        self.on_dark_mode_changed()

    def on_dark_mode_changed(self, *args):
        Gtk.Settings.get_default().set_property(
            'gtk-application-prefer-dark-theme',
            self.confman.conf['dark_mode']
        )

    def emit_destroy(self, *args):
        self.emit('destroy')

    def on_destroy(self, *args):
        alloc = self.get_allocation()
        self.confman.conf['windowsize'] = {
            'width': alloc.width,
            'height': alloc.height
        }
        self.hide()
        self.confman.save_conf()

    def do_startup(self):
        pass
