from subprocess import run
from threading import Thread
from gi.repository import GLib
from datetime import datetime


def new_scrot_name():
    dt = str(datetime.now())
    for c in (' ', ':', '.'):
        dt = dt.replace(c, '-')
    return f'{dt}.png'


def grim_screenshot(window, scrot_dir):
    window.hide()
    scrot_dir = scrot_dir.replace('"', '\"')

    def af():
        res = run(f'grim "{scrot_dir}/{new_scrot_name()}"', shell=True)
        GLib.idle_add(cb, res.returncode)

    def cb(ret_code):
        window.show()
        if ret_code != 0:
            # TODO: handle errors
            print(ret_code)

    Thread(target=af, daemon=True).start()


def grim_selection_screenshot(window, scrot_dir):
    window.hide()
    scrot_dir = scrot_dir.replace('"', '\"')

    def af():
        res = run(
            f'grim -g "$(slurp)" "{scrot_dir}/{new_scrot_name()}"', shell=True
        )
        GLib.idle_add(cb, res.returncode)

    def cb(ret_code):
        window.show()
        if ret_code != 0:
            # TODO: handle errors
            print(ret_code)

    Thread(target=af, daemon=True).start()
