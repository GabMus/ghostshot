# GhostShot

Screenshot app for wlroots based compositors, based on [grim](https://github.com/emersion/grim).

# Dependencies

- gtk4 >= 4.3
- libadwaita >= 1.0
- python >= 3.8

# Downloading the latest development snapshot (`x86_64` only)

- Head over to [the *Jobs* page of the repository](https://gitlab.gnome.org/gabmus/ghostshot/-/jobs)
- find the latest job with name *flatpak*
- press the download button on the right
- extract the zip you just downloaded (typically called `Flatpak_artifacts.zip`) and open a terminal in the resulting directory
- install the snapshot with `flatpak --user install ghostshot-dev.flatpak`

# Running from source

This application targets Flatpak, therefore that's the recommended way to run the app.

To run the app from source using Flatpak:

- install and open GNOME Builder
- select the *Clone Repository...* button
- paste in the url of this repository, then select *Clone Project*
- press the *Run* button indicated by a play icon (like this one `▶`) in the headerbar or press `Ctrl+F5` on your keyboard
